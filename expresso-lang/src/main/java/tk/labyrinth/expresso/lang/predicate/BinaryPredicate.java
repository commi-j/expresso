package tk.labyrinth.expresso.lang.predicate;

import tk.labyrinth.expresso.lang.expression.BinaryExpression;

/**
 * Generic interface for expression that takes two arguments and may be resolved into {@link Boolean}.
 */
public interface BinaryPredicate<F, S> extends BinaryExpression<F, S, Boolean>, Predicate {

	BinaryPredicate<F, S> withFirst(F first);

	BinaryPredicate<F, S> withSecond(S second);
}
