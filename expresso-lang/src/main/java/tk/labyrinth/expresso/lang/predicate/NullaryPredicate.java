package tk.labyrinth.expresso.lang.predicate;

import tk.labyrinth.expresso.lang.expression.NullaryExpression;

/**
 * Generic interface for expression that takes no arguments and may be resolved into {@link Boolean}.
 */
public interface NullaryPredicate extends Predicate, NullaryExpression<Boolean> {
	// empty
}
