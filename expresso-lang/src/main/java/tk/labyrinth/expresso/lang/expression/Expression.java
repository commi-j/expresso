package tk.labyrinth.expresso.lang.expression;

/**
 * Generic interface for expression that may be resolved into {@link R}.
 */
public interface Expression<R> {
	// empty
}
