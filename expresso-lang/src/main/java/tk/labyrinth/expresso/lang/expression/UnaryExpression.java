package tk.labyrinth.expresso.lang.expression;

/**
 * Generic interface for expression that takes one argument and may be resolved into {@link R}.
 */
public interface UnaryExpression<F, R> extends Expression<R> {

	F first();
}
