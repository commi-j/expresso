package tk.labyrinth.expresso.lang.expression;

/**
 * Generic interface for expression that takes no arguments and may be resolved into {@link R}.
 */
public interface NullaryExpression<R> {
	// empty
}
