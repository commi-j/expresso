package tk.labyrinth.expresso.lang.operator;

public enum StringOperator {
	CONTAINS,
	ENDS_WITH,
	STARTS_WITH
}
