package tk.labyrinth.expresso.lang.expression;

/**
 * Generic interface for expression that takes two arguments and may be resolved into {@link R}.
 */
public interface BinaryExpression<F, S, R> extends Expression<R> {

	F first();

	S second();
}
