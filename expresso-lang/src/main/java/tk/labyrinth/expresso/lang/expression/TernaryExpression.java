package tk.labyrinth.expresso.lang.expression;

/**
 * Generic interface for expression that takes three arguments and may be resolved into {@link R}.
 */
public interface TernaryExpression<F, S, T, R> extends Expression<R> {

	F first();

	S second();

	T third();
}
