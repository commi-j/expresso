package tk.labyrinth.expresso.lang.operator;

public enum JunctionOperator {
	AND,
	OR
}
