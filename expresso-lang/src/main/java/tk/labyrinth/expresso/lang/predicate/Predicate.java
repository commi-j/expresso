package tk.labyrinth.expresso.lang.predicate;

import tk.labyrinth.expresso.lang.expression.Expression;

/**
 * Generic interface for expression that may be resolved into {@link Boolean}.
 */
public interface Predicate extends Expression<Boolean> {
	// empty
}
