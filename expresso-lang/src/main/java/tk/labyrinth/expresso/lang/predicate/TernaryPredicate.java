package tk.labyrinth.expresso.lang.predicate;

import tk.labyrinth.expresso.lang.expression.TernaryExpression;

/**
 * Generic interface for expression that takes three arguments and may be resolved into {@link Boolean}.
 */
public interface TernaryPredicate<F, S, T> extends TernaryExpression<F, S, T, Boolean>, Predicate {
	// empty
}
