package tk.labyrinth.expresso.lang.predicate;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import tk.labyrinth.expresso.lang.operator.JunctionOperator;

import java.util.List;

/**
 * {@link Predicate} that consists of {@link JunctionOperator} and zero or more other predicates.
 */
@Accessors(fluent = true)
@Data
public final class JunctionPredicate implements Predicate {

	@NonNull
	private final JunctionOperator operator;

	@NonNull
	private final List<Predicate> predicates;
}
