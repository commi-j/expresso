package tk.labyrinth.expresso.lang.predicate;

import tk.labyrinth.expresso.lang.expression.UnaryExpression;

/**
 * Generic interface for expression that takes one argument and may be resolved into {@link Boolean}.
 */
public interface UnaryPredicate<F> extends Predicate, UnaryExpression<F, Boolean> {
	// empty
}
