package tk.labyrinth.expresso.query.domain.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.SearchQuery;
import tk.labyrinth.expresso.query.test.Person;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @deprecated TODO: Rework into interfaces.
 */
@Deprecated
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class AbstractQueryExecutorTest<C> {

	@BeforeAll
	private void setUp() throws Exception {
		setUp(Person.data());
	}

	@Test
	void testGreaterThan() {
		SearchQuery<Person> query = SearchQuery.builder(Person.class)
				.predicate(Predicates.greaterThan("birthDate", LocalDate.of(1909, 4, 30)))
				.build();
		//
		Assertions.assertEquals(2, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Arrays.asList(14, 15),
				getExecutor().search(getContext(), query).stream().map(Person::getUid).collect(Collectors.toList()));
	}

	@Test
	void testGreaterThanOrEqualTo() {
		SearchQuery<Person> query = SearchQuery.builder(Person.class)
				.predicate(Predicates.greaterThanOrEqualTo("birthDate", LocalDate.of(1909, 4, 30)))
				.build();
		//
		Assertions.assertEquals(3, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Arrays.asList(13, 14, 15),
				getExecutor().search(getContext(), query).stream().map(Person::getUid).collect(Collectors.toList()));
	}

	@Test
	void testSearchWithLimit() {
		SearchQuery<Person> query = SearchQuery.builder(Person.class)
				.limit(2L)
				.build();
		//
		Assertions.assertEquals(2, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Arrays.asList(0, 1),
				getExecutor().search(getContext(), query).stream().map(Person::getUid).collect(Collectors.toList()));
	}

	@Test
	void testSearchWithOffset() {
		SearchQuery<Person> query = SearchQuery.builder(Person.class)
				.offset(14L)
				.build();
		//
		Assertions.assertEquals(3, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Arrays.asList(14, 15, 16),
				getExecutor().search(getContext(), query).stream().map(Person::getUid).collect(Collectors.toList()));
	}

	@Test
	void testSearchWithOffsetAndLimit() {
		SearchQuery<Person> query = SearchQuery.builder(Person.class)
				.offset(2L)
				.limit(3L)
				.build();
		//
		Assertions.assertEquals(3, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Arrays.asList(2, 3, 4),
				getExecutor().search(getContext(), query).stream().map(Person::getUid).collect(Collectors.toList()));
	}

	protected abstract C getContext();

	protected abstract QueryExecutor<C> getExecutor();

	protected abstract void setUp(Stream<Person> data) throws Exception;
}
