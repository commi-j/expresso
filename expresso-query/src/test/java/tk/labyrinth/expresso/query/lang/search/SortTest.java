package tk.labyrinth.expresso.query.lang.search;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

class SortTest {

	@Test
	void testAscending() {
		Assertions.assertEquals(new Sort(Collections.singletonList(new Sort.Entry(Sort.Direction.ASCENDING, "prop"))),
				Sort.ascending("prop"));
		//
		Assertions.assertThrows(NullPointerException.class, () -> Sort.ascending(null));
	}

	@Test
	void testBy() {
		Assertions.assertEquals(new Sort(Collections.singletonList(new Sort.Entry(Sort.Direction.ASCENDING, "prop"))),
				Sort.by("prop", Sort.Direction.ASCENDING));
		Assertions.assertEquals(new Sort(Collections.singletonList(new Sort.Entry(Sort.Direction.DESCENDING, "prop"))),
				Sort.by("prop", Sort.Direction.DESCENDING));
		//
		Assertions.assertThrows(NullPointerException.class, () -> Sort.by(null, Sort.Direction.ASCENDING));
		Assertions.assertThrows(NullPointerException.class, () -> Sort.by("prop", null));
	}

	@Test
	void testDescending() {
		Assertions.assertEquals(new Sort(Collections.singletonList(new Sort.Entry(Sort.Direction.DESCENDING, "prop"))),
				Sort.descending("prop"));
		//
		Assertions.assertThrows(NullPointerException.class, () -> Sort.descending(null));
	}

	@Test
	void testThenAscending() {
		Assertions.assertEquals(new Sort(Arrays.asList(
				new Sort.Entry(Sort.Direction.DESCENDING, "prop0"),
				new Sort.Entry(Sort.Direction.ASCENDING, "prop1"))),
				Sort.descending("prop0").thenAscending("prop1"));
		//
		Assertions.assertThrows(NullPointerException.class, () -> Sort.descending("prop0").thenAscending(null));
	}

	@Test
	void testThenBy() {
		Assertions.assertEquals(new Sort(Arrays.asList(
				new Sort.Entry(Sort.Direction.ASCENDING, "prop0"),
				new Sort.Entry(Sort.Direction.ASCENDING, "prop1"))),
				Sort.ascending("prop0").thenBy("prop1", Sort.Direction.ASCENDING));
		Assertions.assertEquals(new Sort(Arrays.asList(
				new Sort.Entry(Sort.Direction.DESCENDING, "prop0"),
				new Sort.Entry(Sort.Direction.DESCENDING, "prop1"))),
				Sort.descending("prop0").thenBy("prop1", Sort.Direction.DESCENDING));
		//
		Assertions.assertThrows(NullPointerException.class, () -> Sort.ascending("prop0").thenBy(null, Sort.Direction.ASCENDING));
		Assertions.assertThrows(NullPointerException.class, () -> Sort.descending("prop0").thenBy("prop0", null));
	}

	@Test
	void testThenDescending() {
		Assertions.assertEquals(new Sort(Arrays.asList(
				new Sort.Entry(Sort.Direction.ASCENDING, "prop0"),
				new Sort.Entry(Sort.Direction.DESCENDING, "prop1"))),
				Sort.ascending("prop0").thenDescending("prop1"));
		//
		Assertions.assertThrows(NullPointerException.class, () -> Sort.ascending("prop0").thenDescending(null));
	}
}
