package tk.labyrinth.expresso.query.domain.java;

import tk.labyrinth.expresso.query.domain.common.QueryExecutor;
import tk.labyrinth.expresso.query.domain.common.QueryExecutorStringTestBase;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JavaQueryExecutorStringTest implements QueryExecutorStringTestBase<JavaContext> {

	private final JavaQueryExecutor queryExecutor = new JavaQueryExecutor(new SpringPropertyAccessor());

	private Map<Class<?>, List<?>> data = null;

	@Override
	public JavaContext getContext() {
		return new TestContext();
	}

	@Override
	public QueryExecutor<JavaContext> getExecutor() {
		return queryExecutor;
	}

	@Override
	public void setUp(Map<Class<?>, Stream<?>> data) {
		this.data = data.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().collect(Collectors.toList())));
	}

	private class TestContext implements JavaContext {

		@Override
		@SuppressWarnings("unchecked")
		public <T> Stream<T> getStream(Class<T> type) {
			return (Stream<T>) data.get(type).stream();
		}
	}
}
