package tk.labyrinth.expresso.query.domain.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import org.springframework.data.mongodb.core.MongoTemplate;
import tk.labyrinth.expresso.query.domain.common.QueryExecutor;
import tk.labyrinth.expresso.query.domain.common.QueryExecutorStringTestBase;

import java.util.Map;
import java.util.stream.Stream;

public class MongoDbQueryExecutorStringTest implements QueryExecutorStringTestBase<MongoDbContext> {

	private final MongoDbQueryExecutor queryExecutor = new MongoDbQueryExecutor();

	private MongoTemplate template = null;

	@Override
	public MongoDbContext getContext() {
		return new MongoDbContext(template);
	}

	@Override
	public QueryExecutor<MongoDbContext> getExecutor() {
		return queryExecutor;
	}

	@Override
	public void setUp(Map<Class<?>, Stream<?>> data) {
		MongoServer server = new MongoServer(new MemoryBackend());
		MongoClient client = new MongoClient(new ServerAddress(server.bind()));
		template = new MongoTemplate(client, "testdb");
		data.values().forEach(typeData -> typeData.forEach(template::save));
	}
}
