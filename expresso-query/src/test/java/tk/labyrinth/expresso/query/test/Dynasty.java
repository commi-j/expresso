package tk.labyrinth.expresso.query.test;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@AllArgsConstructor
@Builder
@Data
@Document
@Entity
@NoArgsConstructor
public class Dynasty {

	private String name = null;

	@javax.persistence.Id
	@org.springframework.data.annotation.Id
	private Integer uid = null;

	public static Dynasty byName(String name) {
		return data().filter(dynasty -> Objects.equals(dynasty.getName(), name)).findFirst().orElseThrow(() -> new IllegalArgumentException(name));
	}

	public static Stream<Dynasty> data() {
		AtomicInteger idSequence = new AtomicInteger(0);
		return Stream.of(
				Dynasty.builder().name("Carolingiens").uid(idSequence.getAndIncrement()).build(),
				Dynasty.builder().name("Habsburg").uid(idSequence.getAndIncrement()).build(),
				Dynasty.builder().name("Luxemburg").uid(idSequence.getAndIncrement()).build(),
				Dynasty.builder().name("Orange-Nassau").uid(idSequence.getAndIncrement()).build(),
				Dynasty.builder().name(null).uid(idSequence.getAndIncrement()).build()
		);
	}
}
