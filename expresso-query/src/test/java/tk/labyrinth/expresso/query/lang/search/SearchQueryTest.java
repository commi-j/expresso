package tk.labyrinth.expresso.query.lang.search;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SearchQueryTest {

	@Test
	void all() {
		Assertions.assertEquals(SearchQuery.all(Object.class),
				SearchQuery.builder(Object.class).build());
	}

	@Test
	void builderLimit() {
		Assertions.assertAll(
				() -> Assertions.assertEquals(Long.valueOf(12), SearchQuery.builder(Object.class)
						.limit(12L).build().getLimit()),
				() -> Assertions.assertNull(SearchQuery.builder(Object.class)
						.limit(null).build().getLimit())
		);
	}

	@Test
	void builderOffset() {
		Assertions.assertAll(
				() -> Assertions.assertEquals(Long.valueOf(12), SearchQuery.builder(Object.class)
						.offset(12L).build().getOffset()),
				() -> Assertions.assertNull(SearchQuery.builder(Object.class)
						.offset(null).build().getOffset())
		);
	}

	@Test
	void limit() {
		Assertions.assertAll(
				() -> Assertions.assertEquals(SearchQuery.limit(Object.class, 50L),
						SearchQuery.builder(Object.class).limit(50L).build()),
				() -> Assertions.assertEquals(SearchQuery.limit(Object.class, null),
						SearchQuery.builder(Object.class).build())
		);
	}

	@Test
	void limitOffset() {
		Assertions.assertAll(
				() -> Assertions.assertEquals(SearchQuery.limitOffset(Object.class, 50L, 200L),
						SearchQuery.builder(Object.class).limit(50L).offset(200L).build()),
				() -> Assertions.assertEquals(SearchQuery.limitOffset(Object.class, null, null),
						SearchQuery.builder(Object.class).build())
		);
	}

	@Test
	void offset() {
		Assertions.assertAll(
				() -> Assertions.assertEquals(SearchQuery.offset(Object.class, 200L),
						SearchQuery.builder(Object.class).offset(200L).build()),
				() -> Assertions.assertEquals(SearchQuery.offset(Object.class, null),
						SearchQuery.builder(Object.class).build())
		);
	}
}
