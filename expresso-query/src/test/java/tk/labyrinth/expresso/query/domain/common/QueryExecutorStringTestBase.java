package tk.labyrinth.expresso.query.domain.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.SearchQuery;
import tk.labyrinth.expresso.query.test.Dynasty;
import tk.labyrinth.expresso.query.test.Person;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

public interface QueryExecutorStringTestBase<C> extends QueryExecutorTestBase<C> {

	@Test
	default void differenceBetweenContainsAndEndsWith() {
		String value = "x";
		{
			SearchQuery<Person> query = SearchQuery.builder(Person.class)
					.predicate(Predicates.contains("name", value, false))
					.build();
			//
			Assertions.assertEquals(Arrays.asList("Beatrix", "Willem-Alexander"),
					getExecutor().search(getContext(), query).stream().map(Person::getName).collect(Collectors.toList()));
		}
		{
			SearchQuery<Person> query = SearchQuery.builder(Person.class)
					.predicate(Predicates.endsWith("name", value, false))
					.build();
			//
			Assertions.assertEquals(Collections.singletonList("Beatrix"),
					getExecutor().search(getContext(), query).stream().map(Person::getName).collect(Collectors.toList()));
		}
	}

	@Test
	default void differenceBetweenContainsAndStartsWith() {
		String value = "s";
		{
			SearchQuery<Person> query = SearchQuery.builder(Person.class)
					.predicate(Predicates.contains("name", value, false))
					.build();
			//
			Assertions.assertEquals(Arrays.asList("Charles I", "Louis I", "Louis II", "Charles II", "Charles III", "Sigismund"),
					getExecutor().search(getContext(), query).stream().map(Person::getName).collect(Collectors.toList()));
		}
		{
			SearchQuery<Person> query = SearchQuery.builder(Person.class)
					.predicate(Predicates.startsWith("name", value, false))
					.build();
			//
			Assertions.assertEquals(Collections.singletonList("Sigismund"),
					getExecutor().search(getContext(), query).stream().map(Person::getName).collect(Collectors.toList()));
		}
	}

	@Test
	default void dynastiesWithNameContainingCaseInsensitiveL() {
		SearchQuery<Dynasty> query = SearchQuery.builder(Dynasty.class)
				.predicate(Predicates.contains("name", "L", false))
				.build();
		//
		Assertions.assertEquals(2, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Arrays.asList("Carolingiens", "Luxemburg"),
				getExecutor().search(getContext(), query).stream().map(Dynasty::getName).collect(Collectors.toList()));
	}

	@Test
	default void dynastiesWithNameContainingLowercaseL() {
		SearchQuery<Dynasty> query = SearchQuery.builder(Dynasty.class)
				.predicate(Predicates.contains("name", "l", true))
				.build();
		//
		Assertions.assertEquals(1, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Collections.singletonList("Carolingiens"),
				getExecutor().search(getContext(), query).stream().map(Dynasty::getName).collect(Collectors.toList()));
	}

	@Test
	default void dynastiesWithNameContainingUppercaseL() {
		SearchQuery<Dynasty> query = SearchQuery.builder(Dynasty.class)
				.predicate(Predicates.contains("name", "L", true))
				.build();
		//
		Assertions.assertEquals(1, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Collections.singletonList("Luxemburg"),
				getExecutor().search(getContext(), query).stream().map(Dynasty::getName).collect(Collectors.toList()));
	}

	@Test
	default void personsWithNameEndingWithII() {
		SearchQuery<Person> query = SearchQuery.builder(Person.class)
				.predicate(Predicates.endsWith("name", "II", true))
				.build();
		//
		Assertions.assertEquals(7, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Arrays.asList("Louis II", "Charles II", "Charles III", "Friedrich III", "Heinrich VII", "Willem II", "Willem III"),
				getExecutor().search(getContext(), query).stream().map(Person::getName).collect(Collectors.toList()));
	}

	@Test
	default void personsWithNameEndingWithSpaceAndII() {
		SearchQuery<Person> query = SearchQuery.builder(Person.class)
				.predicate(Predicates.endsWith("name", " II", true))
				.build();
		//
		Assertions.assertEquals(3, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Arrays.asList("Louis II", "Charles II", "Willem II"),
				getExecutor().search(getContext(), query).stream().map(Person::getName).collect(Collectors.toList()));
	}

	@Test
	default void personsWithNameStartingWithWi() {
		SearchQuery<Person> query = SearchQuery.builder(Person.class)
				.predicate(Predicates.startsWith("name", "Wi", true))
				.build();
		//
		Assertions.assertEquals(4, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Arrays.asList("Willem II", "Willem III", "Wilhelmina", "Willem-Alexander"),
				getExecutor().search(getContext(), query).stream().map(Person::getName).collect(Collectors.toList()));
	}
}
