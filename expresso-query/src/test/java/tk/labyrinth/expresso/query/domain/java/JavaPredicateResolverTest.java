package tk.labyrinth.expresso.query.domain.java;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;

class JavaPredicateResolverTest {

	@Test
	void testFlatAnd() {
		Assertions.assertEquals(
				Predicates.and(
						Predicates.equalTo("a", "b"),
						Predicates.equalTo("c", "d")),
				Predicates.flatAnd(
						Predicates.equalTo("a", "b"),
						Predicates.equalTo("c", "d")));
		Assertions.assertEquals(
				Predicates.and(
						Predicates.equalTo("a", "b"),
						Predicates.equalTo("c", "d"),
						Predicates.equalTo("e", "f")),
				Predicates.flatAnd(
						Predicates.equalTo("a", "b"),
						Predicates.and(
								Predicates.equalTo("c", "d"),
								Predicates.equalTo("e", "f"))));
	}

	@Test
	void testPrepareString() {
		{
			Assertions.assertEquals("", JavaPredicateResolver.prepareString("", true));
			Assertions.assertEquals(" ", JavaPredicateResolver.prepareString(" ", true));
			Assertions.assertEquals("aa", JavaPredicateResolver.prepareString("aa", true));
			Assertions.assertEquals("aA", JavaPredicateResolver.prepareString("aA", true));
			Assertions.assertEquals("AA", JavaPredicateResolver.prepareString("AA", true));
			Assertions.assertNull(JavaPredicateResolver.prepareString(null, true));
		}
		{
			Assertions.assertEquals("", JavaPredicateResolver.prepareString("", false));
			Assertions.assertEquals(" ", JavaPredicateResolver.prepareString(" ", false));
			Assertions.assertEquals("aa", JavaPredicateResolver.prepareString("aa", false));
			Assertions.assertEquals("aa", JavaPredicateResolver.prepareString("aA", false));
			Assertions.assertEquals("aa", JavaPredicateResolver.prepareString("AA", false));
			Assertions.assertNull(JavaPredicateResolver.prepareString(null, false));
		}
	}
}
