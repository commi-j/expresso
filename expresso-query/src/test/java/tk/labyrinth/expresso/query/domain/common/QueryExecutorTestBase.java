package tk.labyrinth.expresso.query.domain.common;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import tk.labyrinth.expresso.query.test.Dynasty;
import tk.labyrinth.expresso.query.test.Person;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public interface QueryExecutorTestBase<C> {

	C getContext();

	QueryExecutor<C> getExecutor();

	@BeforeAll
	default void setUp() throws Exception {
		Map<Class<?>, Stream<?>> data = new HashMap<>();
		data.put(Dynasty.class, Dynasty.data());
		data.put(Person.class, Person.data());
		setUp(data);
	}

	void setUp(Map<Class<?>, Stream<?>> data) throws Exception;
}
