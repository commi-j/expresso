package tk.labyrinth.expresso.query.domain.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.dialect.HSQLDialect;
import org.hsqldb.jdbc.JDBCPool;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import tk.labyrinth.expresso.query.domain.common.QueryExecutor;
import tk.labyrinth.expresso.query.domain.common.QueryExecutorStringTestBase;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Stream;

public class HibernateQueryExecutorStringTest implements QueryExecutorStringTestBase<HibernateContext> {

	private final HibernateQueryExecutor queryExecutor = new HibernateQueryExecutor();

	private SessionFactory sessionFactory = null;

	private Session session = null;

	private SessionFactory buildSessionFactory() throws IOException {
		LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
		{
			JDBCPool dataSource = new JDBCPool();
			dataSource.setUrl("jdbc:hsqldb:mem:testdb");
			localSessionFactoryBean.setDataSource(dataSource);
		}
		{
			Properties properties = new Properties();
			properties.setProperty(AvailableSettings.DIALECT, HSQLDialect.class.getName());
			properties.setProperty(AvailableSettings.HBM2DDL_AUTO, "update");
			localSessionFactoryBean.setHibernateProperties(properties);
		}
		localSessionFactoryBean.setPackagesToScan("tk.labyrinth.expresso.query.test");
		localSessionFactoryBean.afterPropertiesSet();
		return localSessionFactoryBean.getObject();
	}

	@AfterEach
	void closeSession() {
		session.getTransaction().commit();
		session.close();
		session = null;
	}

	@BeforeEach
	void openSession() {
		session = sessionFactory.openSession();
		session.beginTransaction();
	}

	@Override
	public HibernateContext getContext() {
		return new HibernateContext(session);
	}

	@Override
	public QueryExecutor<HibernateContext> getExecutor() {
		return queryExecutor;
	}

	@Override
	public void setUp(Map<Class<?>, Stream<?>> data) throws IOException {
		sessionFactory = buildSessionFactory();
		openSession();
		data.values().forEach(typeData -> typeData.forEach(session::saveOrUpdate));
		closeSession();
	}
}
