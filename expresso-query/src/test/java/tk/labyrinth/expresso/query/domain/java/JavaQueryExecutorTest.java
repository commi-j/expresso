package tk.labyrinth.expresso.query.domain.java;

import tk.labyrinth.expresso.query.domain.common.AbstractQueryExecutorTest;
import tk.labyrinth.expresso.query.domain.common.QueryExecutor;
import tk.labyrinth.expresso.query.test.Person;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JavaQueryExecutorTest extends AbstractQueryExecutorTest<JavaContext> {

	private final JavaQueryExecutor queryExecutor = new JavaQueryExecutor(new SpringPropertyAccessor());

	private List<Person> humans = null;

	@Override
	protected JavaContext getContext() {
		return new TestContext();
	}

	@Override
	protected QueryExecutor<JavaContext> getExecutor() {
		return queryExecutor;
	}

	@Override
	protected void setUp(Stream<Person> data) throws Exception {
		humans = data.collect(Collectors.toList());
	}

	private class TestContext implements JavaContext {

		@Override
		@SuppressWarnings("unchecked")
		public <T> Stream<T> getStream(Class<T> type) {
			return (Stream<T>) humans.stream();
		}
	}
}
