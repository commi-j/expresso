package tk.labyrinth.expresso.query.domain.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.SearchQuery;
import tk.labyrinth.expresso.query.lang.search.Sort;
import tk.labyrinth.expresso.query.test.Dynasty;
import tk.labyrinth.expresso.query.test.Person;
import tk.labyrinth.expresso.query.test.Sex;

import java.util.Arrays;
import java.util.stream.Collectors;

public interface QueryExecutorObjectTestBase<C> extends QueryExecutorTestBase<C> {

	@Test
	default void personsSortedAscendingByNameLimitedBy5() {
		SearchQuery<Person> query = SearchQuery.builder(Person.class)
				.limit(5L)
				.sort(Sort.ascending("name"))
				.build();
		//
		Assertions.assertEquals(5, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Arrays.asList(null, "Beatrix", "Charles I", "Charles II", "Charles III"),
				getExecutor().search(getContext(), query).stream().map(Person::getName).collect(Collectors.toList()));
	}

	@Test
	default void personsSortedAscendingBySexDescendingByBirthDateLimitedBy5() {
		SearchQuery<Person> query = SearchQuery.builder(Person.class)
				.limit(5L)
				.sort(Sort.ascending("sex").thenDescending("birthDate"))
				.build();
		//
		Assertions.assertEquals(5, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Arrays.asList(null, "Beatrix", "Juliana", "Wilhelmina", "Willem-Alexander"),
				getExecutor().search(getContext(), query).stream().map(Person::getName).collect(Collectors.toList()));
	}

	@Test
	default void personsWithDynastyEqualToLuxemburg() {
		SearchQuery<Person> query = SearchQuery.builder(Person.class)
				.predicate(Predicates.equalTo("dynastyUid", Dynasty.byName("Luxemburg").getUid()))
				.build();
		//
		Assertions.assertEquals(3, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Arrays.asList("Heinrich VII", "Karl IV", "Sigismund"),
				getExecutor().search(getContext(), query).stream().map(Person::getName).collect(Collectors.toList()));
	}

	@Test
	default void personsWithDynastyInHabsburgOrLuxemburg() {
		SearchQuery<Person> query = SearchQuery.builder(Person.class)
				.predicate(Predicates.in("dynastyUid",
						Dynasty.byName("Habsburg").getUid(), Dynasty.byName("Luxemburg").getUid()))
				.build();
		//
		Assertions.assertEquals(Arrays.asList("Friedrich III", "Heinrich VII", "Karl IV", "Sigismund"),
				getExecutor().search(getContext(), query).stream()
						.map(Person::getName)
						.collect(Collectors.toList()));
	}

	@Test
	default void personsWithSexEqualToFemale() {
		SearchQuery<Person> query = SearchQuery.builder(Person.class)
				.predicate(Predicates.equalTo("sex", Sex.FEMALE))
				.build();
		//
		Assertions.assertEquals(3, getExecutor().count(getContext(), query));
		Assertions.assertEquals(Arrays.asList("Wilhelmina", "Juliana", "Beatrix"),
				getExecutor().search(getContext(), query).stream().map(Person::getName).collect(Collectors.toList()));
	}
}
