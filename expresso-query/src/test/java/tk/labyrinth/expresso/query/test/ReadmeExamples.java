package tk.labyrinth.expresso.query.test;

import tk.labyrinth.expresso.query.domain.common.QueryExecutor;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.SearchQuery;
import tk.labyrinth.expresso.query.lang.search.Sort;

import java.time.LocalDate;
import java.util.List;

@SuppressWarnings("unused")
public class ReadmeExamples<C> {

	/**
	 * This text is copied to README.md.
	 */
	private ReadmeExamples(QueryExecutor<C> queryExecutor, C context) {
		List<MyObject> equalToSearchResult = queryExecutor.search(context, SearchQuery.builder(MyObject.class)
				.predicate(Predicates.equalTo("someField", "someValue"))
				.build());
		//
		List<MyObject> caseInsensitiveContainsSearchResult = queryExecutor.search(context, SearchQuery.builder(MyObject.class)
				.predicate(Predicates.contains("someField", "someValue", false))
				.build());
		//
		List<MyObject> pageResult = queryExecutor.search(context, SearchQuery.builder(MyObject.class)
				.sort(Sort.ascending("groupId").thenDescending("name"))
				.offset(200L)
				.limit(50L)
				.build());
		//
		long count = queryExecutor.count(context, SearchQuery.builder(MyObject.class)
				.predicate(Predicates.and(
						Predicates.greaterThanOrEqualTo("someDate", LocalDate.of(2018, 12, 30)),
						Predicates.lessThan("someDate", LocalDate.of(2019, 1, 10))))
				.build());
	}

	private static class MyObject {
		// empty
	}
}
