package tk.labyrinth.expresso.query.domain.common.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class QueryExecutorUtilsTest {

	@Test
	void testAdjustCount() {
		Assertions.assertEquals(7, QueryExecutorUtils.adjustCount(7, null, null));
		Assertions.assertEquals(3, QueryExecutorUtils.adjustCount(7, 4L, null));
		Assertions.assertEquals(2, QueryExecutorUtils.adjustCount(7, null, 2L));
		Assertions.assertEquals(2, QueryExecutorUtils.adjustCount(7, 4L, 2L));
		Assertions.assertEquals(1, QueryExecutorUtils.adjustCount(7, 6L, 2L));
		//
		Assertions.assertEquals(0, QueryExecutorUtils.adjustCount(7, 7L, null));
		Assertions.assertEquals(0, QueryExecutorUtils.adjustCount(7, 8L, null));
		Assertions.assertEquals(0, QueryExecutorUtils.adjustCount(7, null, 0L));
		Assertions.assertEquals(0, QueryExecutorUtils.adjustCount(7, 7L, 1L));
	}
}
