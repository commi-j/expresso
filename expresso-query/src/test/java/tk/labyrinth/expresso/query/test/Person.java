package tk.labyrinth.expresso.query.test;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@AllArgsConstructor
@Builder
@Data
@Document
@Entity
@NoArgsConstructor
public class Person {

	private LocalDate birthDate = null;

	private Integer dynastyUid = null;

	private String name = null;

	private Sex sex = null;

	@javax.persistence.Id
	@org.springframework.data.annotation.Id
	private Integer uid = null;

	public static Stream<Person> data() {
		AtomicInteger idSequence = new AtomicInteger(0);
		return Stream.of(
				// Carolingiens
				Person.builder().dynastyUid(Dynasty.byName("Carolingiens").getUid()).name("Charles I").birthDate(LocalDate.of(747, 4, 2)).sex(Sex.MALE).uid(idSequence.getAndIncrement()).build(),
				Person.builder().dynastyUid(Dynasty.byName("Carolingiens").getUid()).name("Louis I").birthDate(LocalDate.of(778, 1, 1)).sex(Sex.MALE).uid(idSequence.getAndIncrement()).build(),
				Person.builder().dynastyUid(Dynasty.byName("Carolingiens").getUid()).name("Lothaire I").birthDate(LocalDate.of(795, 1, 1)).sex(Sex.MALE).uid(idSequence.getAndIncrement()).build(),
				Person.builder().dynastyUid(Dynasty.byName("Carolingiens").getUid()).name("Louis II").birthDate(LocalDate.of(825, 1, 1)).sex(Sex.MALE).uid(idSequence.getAndIncrement()).build(),
				Person.builder().dynastyUid(Dynasty.byName("Carolingiens").getUid()).name("Charles II").birthDate(LocalDate.of(823, 6, 13)).sex(Sex.MALE).uid(idSequence.getAndIncrement()).build(),
				Person.builder().dynastyUid(Dynasty.byName("Carolingiens").getUid()).name("Charles III").birthDate(LocalDate.of(839, 6, 13)).sex(Sex.MALE).uid(idSequence.getAndIncrement()).build(),
				// Habsburg
				Person.builder().dynastyUid(Dynasty.byName("Habsburg").getUid()).name("Friedrich III").birthDate(LocalDate.of(1415, 9, 21)).sex(Sex.MALE).uid(idSequence.getAndIncrement()).build(),
				// TODO:
				// Luxemburg
				Person.builder().dynastyUid(Dynasty.byName("Luxemburg").getUid()).name("Heinrich VII").birthDate(LocalDate.of(1275, 1, 1)).sex(Sex.MALE).uid(idSequence.getAndIncrement()).build(),
				Person.builder().dynastyUid(Dynasty.byName("Luxemburg").getUid()).name("Karl IV").birthDate(LocalDate.of(1316, 5, 14)).sex(Sex.MALE).uid(idSequence.getAndIncrement()).build(),
				Person.builder().dynastyUid(Dynasty.byName("Luxemburg").getUid()).name("Sigismund").birthDate(LocalDate.of(1368, 2, 15)).sex(Sex.MALE).uid(idSequence.getAndIncrement()).build(),
				// Orange-Nassau
				Person.builder().dynastyUid(Dynasty.byName("Orange-Nassau").getUid()).name("Willem II").birthDate(LocalDate.of(1792, 12, 6)).sex(Sex.MALE).uid(idSequence.getAndIncrement()).build(),
				Person.builder().dynastyUid(Dynasty.byName("Orange-Nassau").getUid()).name("Willem III").birthDate(LocalDate.of(1817, 2, 19)).sex(Sex.MALE).uid(idSequence.getAndIncrement()).build(),
				Person.builder().dynastyUid(Dynasty.byName("Orange-Nassau").getUid()).name("Wilhelmina").birthDate(LocalDate.of(1880, 8, 31)).sex(Sex.FEMALE).uid(idSequence.getAndIncrement()).build(),
				Person.builder().dynastyUid(Dynasty.byName("Orange-Nassau").getUid()).name("Juliana").birthDate(LocalDate.of(1909, 4, 30)).sex(Sex.FEMALE).uid(idSequence.getAndIncrement()).build(),
				Person.builder().dynastyUid(Dynasty.byName("Orange-Nassau").getUid()).name("Beatrix").birthDate(LocalDate.of(1938, 1, 31)).sex(Sex.FEMALE).uid(idSequence.getAndIncrement()).build(),
				Person.builder().dynastyUid(Dynasty.byName("Orange-Nassau").getUid()).name("Willem-Alexander").birthDate(LocalDate.of(1967, 4, 27)).sex(Sex.MALE).uid(idSequence.getAndIncrement()).build(),
				// Nullman
				Person.builder().dynastyUid(null).name(null).birthDate(null).sex(null).uid(idSequence.getAndIncrement()).build()
		);
	}
}
