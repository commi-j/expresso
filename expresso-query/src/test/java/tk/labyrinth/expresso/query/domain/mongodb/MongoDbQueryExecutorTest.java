package tk.labyrinth.expresso.query.domain.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import org.springframework.data.mongodb.core.MongoTemplate;
import tk.labyrinth.expresso.query.domain.common.AbstractQueryExecutorTest;
import tk.labyrinth.expresso.query.domain.common.QueryExecutor;
import tk.labyrinth.expresso.query.test.Person;

import java.util.stream.Stream;

public class MongoDbQueryExecutorTest extends AbstractQueryExecutorTest<MongoDbContext> {

	private final MongoDbQueryExecutor queryExecutor = new MongoDbQueryExecutor();

	private MongoTemplate template = null;

	@Override
	protected MongoDbContext getContext() {
		return new MongoDbContext(template);
	}

	@Override
	protected QueryExecutor<MongoDbContext> getExecutor() {
		return queryExecutor;
	}

	@Override
	protected void setUp(Stream<Person> data) {
		MongoServer server = new MongoServer(new MemoryBackend());
		MongoClient client = new MongoClient(new ServerAddress(server.bind()));
		template = new MongoTemplate(client, "testdb");
		data.forEach(template::save);
	}
}
