package tk.labyrinth.expresso.query.domain.java;

import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.domain.common.QueryExecutor;
import tk.labyrinth.expresso.query.domain.common.util.QueryExecutorUtils;
import tk.labyrinth.expresso.query.lang.search.SearchQuery;
import tk.labyrinth.expresso.query.lang.search.Sort;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JavaQueryExecutor implements QueryExecutor<JavaContext> {

	private final JavaPredicateResolver predicateResolver;

	private final JavaPropertyAccessor propertyAccessor;

	public JavaQueryExecutor(JavaPropertyAccessor propertyAccessor) {
		this(propertyAccessor, new JavaPredicateResolver(propertyAccessor));
	}

	public JavaQueryExecutor(JavaPropertyAccessor propertyAccessor, JavaPredicateResolver predicateResolver) {
		this.propertyAccessor = propertyAccessor;
		this.predicateResolver = predicateResolver;
	}

	private <T, P extends Comparable<P>> Comparator<T> buildPropertyComparator(
			Function<T, P> propertyAccessFunction,
			Sort.Direction direction) {
		Comparator<T> result;
		switch (direction) {
			case ASCENDING:
				result = Comparator.comparing(propertyAccessFunction);
				break;
			case DESCENDING:
				result = Comparator.comparing(propertyAccessFunction, Comparator.reverseOrder());
				break;
			default:
				throw new IllegalStateException(String.valueOf(direction));
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	private <T> T cast(Object object) {
		return (T) object;
	}

	public <T> Comparator<T> buildComparatorEntry(Sort.Entry sortEntry) {
		String property = sortEntry.property();
		//
		return buildPropertyComparator(object -> cast(propertyAccessor.get(object, property)), sortEntry.direction());
	}

	@SuppressWarnings("unchecked")
	public <T> java.util.function.Predicate<T> buildPredicate(Predicate predicate) {
		return (java.util.function.Predicate<T>) predicateResolver.resolve(predicate);
	}

	public <T> Comparator<T> buildSort(Sort sort) {
		return cast(sort.entries().stream()
				.map(this::buildComparatorEntry)
				.reduce(Comparator::thenComparing)
				.orElseThrow(IllegalStateException::new));
	}

	public <T> Stream<T> buildStream(JavaContext context, SearchQuery<T> query, boolean search) {
		Stream<T> result = context.getStream(query.getType());
		if (query.getPredicate() != null) {
			result = result.filter(buildPredicate(query.getPredicate()));
		}
		if (search) {
			if (query.getSort() != null) {
				result = result.sorted(buildSort(query.getSort()));
			}
			if (query.getOffset() != null) {
				result = result.skip(query.getOffset());
			}
			if (query.getLimit() != null) {
				result = result.limit(query.getLimit());
			}
		}
		return result;
	}

	@Override
	public long count(JavaContext context, SearchQuery<?> query) {
		return QueryExecutorUtils.adjustCount(
				buildStream(context, query, false).count(),
				query.getOffset(),
				query.getLimit());
	}

	@Override
	public <T> List<T> search(JavaContext context, SearchQuery<T> query) {
		return buildStream(context, query, true).collect(Collectors.toList());
	}
}
