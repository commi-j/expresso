package tk.labyrinth.expresso.query.lang.predicate;

import lombok.Data;
import lombok.experimental.Accessors;
import tk.labyrinth.expresso.lang.operator.StringOperator;
import tk.labyrinth.expresso.lang.predicate.BinaryPredicate;

@Accessors(fluent = true)
@Data
public class StringPropertyPredicate implements BinaryPredicate<String, String> {

	private final boolean caseSensitive;

	private final StringOperator operator;

	private final String property;

	private final String value;

	@Override
	public String first() {
		return property;
	}

	@Override
	public String second() {
		return value;
	}

	@Override
	public StringPropertyPredicate withFirst(String first) {
		return new StringPropertyPredicate(caseSensitive, operator, first, value);
	}

	@Override
	public StringPropertyPredicate withSecond(String second) {
		return new StringPropertyPredicate(caseSensitive, operator, property, second);
	}
}
