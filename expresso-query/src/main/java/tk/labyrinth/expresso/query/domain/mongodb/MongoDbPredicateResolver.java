package tk.labyrinth.expresso.query.domain.mongodb;

import org.springframework.data.mongodb.core.query.Criteria;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.query.domain.common.AbstractPredicateResolver;
import tk.labyrinth.expresso.query.lang.predicate.ObjectPropertyPredicate;
import tk.labyrinth.expresso.query.lang.predicate.StringPropertyPredicate;

public class MongoDbPredicateResolver extends AbstractPredicateResolver<Criteria> {

	{
		register(JunctionPredicate.class, this::resolveJunction);
		register(ObjectPropertyPredicate.class, this::resolveObjectProperty);
		register(StringPropertyPredicate.class, this::resolveStringProperty);
	}

	Criteria resolveJunction(JunctionPredicate predicate) {
		Criteria where = Criteria.where(null);
		Criteria[] criteria = predicate.predicates().stream().map(this::resolve).toArray(Criteria[]::new);
		Criteria result;
		switch (predicate.operator()) {
			case AND:
				result = where.andOperator(criteria);
				break;
			case OR:
				result = where.orOperator(criteria);
				break;
			default:
				throw new RuntimeException(predicate.operator().name());
		}
		return result;
	}

	Criteria resolveObjectProperty(ObjectPropertyPredicate predicate) {
		Criteria where = Criteria.where(predicate.property());
		Object value = predicate.value();
		//
		Criteria result;
		switch (predicate.operator()) {
			case EQUAL_TO:
				result = where.is(value);
				break;
			case GREATER_THAN:
				result = where.gt(value);
				break;
			case GREATER_THAN_OR_EQUAL_TO:
				result = where.gte(value);
				break;
			case IN:
				result = where.in(value);
				break;
			case LESS_THAN:
				result = where.lt(value);
				break;
			case LESS_THAN_OR_EQUAL_TO:
				result = where.lte(value);
				break;
			case NOT_EQUAL_TO:
				result = where.not().is(value);
				break;
			default:
				throw new RuntimeException(predicate.operator().name());
		}
		return result;
	}

	Criteria resolveStringProperty(StringPropertyPredicate predicate) {
		Criteria where = Criteria.where(predicate.property());
		String value = predicate.value();
		String options = predicate.caseSensitive() ? null : "i";
		//
		Criteria result;
		switch (predicate.operator()) {
			case CONTAINS:
				result = where.regex(value, options);
				break;
			case ENDS_WITH:
				result = where.regex(value + "$", options);
				break;
			case STARTS_WITH:
				result = where.regex("^" + value, options);
				break;
			default:
				throw new RuntimeException(predicate.operator().name());
		}
		return result;
	}
}
