package tk.labyrinth.expresso.query.lang.predicate;

import lombok.Data;
import lombok.experimental.Accessors;
import tk.labyrinth.expresso.lang.operator.ObjectOperator;
import tk.labyrinth.expresso.lang.predicate.BinaryPredicate;

@Accessors(fluent = true)
@Data
public final class ObjectPropertyPredicate implements BinaryPredicate<String, Object> {

	private final ObjectOperator operator;

	private final String property;

	private final Object value;

	@Override
	public String first() {
		return property;
	}

	@Override
	public Object second() {
		return value;
	}

	@Override
	public ObjectPropertyPredicate withFirst(String first) {
		return new ObjectPropertyPredicate(operator, first, value);
	}

	@Override
	public ObjectPropertyPredicate withSecond(Object second) {
		return new ObjectPropertyPredicate(operator, property, second);
	}
}
