package tk.labyrinth.expresso.query.lang.search;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import tk.labyrinth.expresso.lang.predicate.Predicate;

/**
 * @param <T> Type
 *
 * @see TypelessSearchQuery
 */
@Data
public class SearchQuery<T> {

	@NonNull
	private final Class<T> type;

	private final Long limit;

	private final Long offset;

	private final Predicate predicate;

	private final Sort sort;

	public TypelessSearchQuery asTypeless() {
		return TypelessSearchQuery.builder()
				.limit(limit)
				.offset(offset)
				.predicate(predicate)
				.sort(sort)
				.build();
	}

	public Sort getSort() {
		// FIXME: Temporal solution to hide QueryExecutor javadoc error.
		return this.sort;
	}

	public Builder<T> toBuilder() {
		return toBuilder(type);
	}

	public <R> Builder<R> toBuilder(Class<R> type) {
		return new Builder<>(type)
				.limit(limit)
				.offset(offset)
				.predicate(predicate)
				.sort(sort);
	}

	public static <T> SearchQuery<T> all(Class<T> type) {
		return SearchQuery.builder(type).build();
	}

	public static <T> Builder<T> builder(Class<T> type) {
		return new Builder<>(type);
	}

	public static <T> SearchQuery<T> limit(Class<T> type, Long limit) {
		return SearchQuery.builder(type).limit(limit).build();
	}

	public static <T> SearchQuery<T> limitOffset(Class<T> type, Long limit, Long offset) {
		return SearchQuery.builder(type).limit(limit).offset(offset).build();
	}

	public static <T> SearchQuery<T> offset(Class<T> type, Long offset) {
		return SearchQuery.builder(type).offset(offset).build();
	}

	@Accessors(fluent = true)
	@Data
	@RequiredArgsConstructor
	public static class Builder<T> {

		@NonNull
		private final Class<T> type;

		private Long limit = null;

		private Long offset = null;

		private Predicate predicate = null;

		private Sort sort = null;

		public SearchQuery<T> build() {
			return new SearchQuery<>(type, limit, offset, predicate, sort);
		}
	}
}
