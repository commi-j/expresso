package tk.labyrinth.expresso.query.lang.predicate;

import tk.labyrinth.expresso.lang.operator.JunctionOperator;
import tk.labyrinth.expresso.lang.operator.ObjectOperator;
import tk.labyrinth.expresso.lang.operator.StringOperator;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Predicates {

	@SafeVarargs
	public static <P extends Predicate> Predicate and(P... predicates) {
		return and(Stream.of(predicates));
	}

	public static Predicate and(Stream<? extends Predicate> predicates) {
		return new JunctionPredicate(JunctionOperator.AND, predicates.collect(Collectors.toList()));
	}

	public static Predicate contains(String property, String value, boolean caseSensitive) {
		return new StringPropertyPredicate(caseSensitive, StringOperator.CONTAINS, property, value);
	}

	/**
	 * @param property non-null, refer to collection
	 * @param value    nullable
	 *
	 * @return non-null
	 */
	public static Predicate contains(String property, Object value) {
		return new ObjectPropertyPredicate(ObjectOperator.CONTAINS, property, value);
	}

	public static Predicate endsWith(String property, String value, boolean caseSensitive) {
		return new StringPropertyPredicate(caseSensitive, StringOperator.ENDS_WITH, property, value);
	}

	public static Predicate equalTo(String property, Object value) {
		return new ObjectPropertyPredicate(ObjectOperator.EQUAL_TO, property, value);
	}

	/**
	 * EXPERIMENTAL: Probably we will add and use #flatten instead.
	 *
	 * @param predicates non-null
	 * @param <P>        PredicateType
	 *
	 * @return non-null
	 *
	 * @since 1.1.4
	 */
	@SafeVarargs
	public static <P extends Predicate> Predicate flatAnd(P... predicates) {
		return flatAnd(Stream.of(predicates));
	}

	/**
	 * EXPERIMENTAL: Probably we will add and use #flatten instead.
	 *
	 * @param predicates non-null
	 *
	 * @return non-null
	 *
	 * @since 1.1.4
	 */
	public static Predicate flatAnd(Stream<Predicate> predicates) {
		return new JunctionPredicate(
				JunctionOperator.AND,
				predicates
						.flatMap(predicate -> isAnd(predicate)
								? ((JunctionPredicate) predicate).predicates().stream()
								: Stream.of(predicate))
						.collect(Collectors.toList()));
	}

	public static Predicate greaterThan(String property, Object value) {
		return new ObjectPropertyPredicate(ObjectOperator.GREATER_THAN, property, value);
	}

	public static Predicate greaterThanOrEqualTo(String property, Object value) {
		return new ObjectPropertyPredicate(ObjectOperator.GREATER_THAN_OR_EQUAL_TO, property, value);
	}

	public static Predicate in(String property, Collection<?> values) {
		return new ObjectPropertyPredicate(ObjectOperator.IN, property, values);
	}

	public static Predicate in(String property, Object... values) {
		return in(property, Arrays.asList(values));
	}

	public static boolean isAnd(Predicate predicate) {
		return predicate instanceof JunctionPredicate &&
				((JunctionPredicate) predicate).operator() == JunctionOperator.AND;
	}

	public static Predicate lessThan(String property, Object value) {
		return new ObjectPropertyPredicate(ObjectOperator.LESS_THAN, property, value);
	}

	public static Predicate lessThanOrEqualTo(String property, Object value) {
		return new ObjectPropertyPredicate(ObjectOperator.LESS_THAN_OR_EQUAL_TO, property, value);
	}

	public static Predicate notEqualTo(String property, Object value) {
		return new ObjectPropertyPredicate(ObjectOperator.NOT_EQUAL_TO, property, value);
	}

	@SafeVarargs
	public static <P extends Predicate> Predicate or(P... predicates) {
		return or(Stream.of(predicates));
	}

	public static Predicate or(Stream<? extends Predicate> predicates) {
		return new JunctionPredicate(JunctionOperator.OR, predicates.collect(Collectors.toList()));
	}

	public static Predicate startsWith(String property, String value, boolean caseSensitive) {
		return new StringPropertyPredicate(caseSensitive, StringOperator.STARTS_WITH, property, value);
	}
}
