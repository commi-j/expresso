package tk.labyrinth.expresso.query.domain.jpa;

import tk.labyrinth.expresso.query.domain.common.QueryExecutor;
import tk.labyrinth.expresso.query.lang.search.SearchQuery;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public abstract class JpaQueryExecutor implements QueryExecutor<JpaContext> {

	private final JpaCriteriaConverter converter = new JpaCriteriaConverter();

	@Override
	public long count(JpaContext context, SearchQuery<?> query) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> List<T> search(JpaContext context, SearchQuery<T> query) {
		CriteriaBuilder criteriaBuilder = context.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(query.getType());
		if (query.getPredicate() != null) {
			// TODO:
//			criteriaQuery = criteriaQuery.where(converter.convertPredicate(criteriaBuilder, query.getPredicate()));
		}
		if (query.getSort() != null) {
			// TODO:
			criteriaQuery = criteriaQuery.orderBy();
		}
		TypedQuery<T> typedQuery = context.getEntityManager().createQuery(criteriaQuery);
		if (query.getLimit() != null) {
			typedQuery.setMaxResults(query.getLimit().intValue());
		}
		if (query.getOffset() != null) {
			typedQuery.setFirstResult(query.getOffset().intValue());
		}
		return typedQuery.getResultList();
	}
}
