package tk.labyrinth.expresso.query.domain.hibernate;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.query.domain.common.AbstractPredicateResolver;
import tk.labyrinth.expresso.query.lang.predicate.ObjectPropertyPredicate;
import tk.labyrinth.expresso.query.lang.predicate.StringPropertyPredicate;

import java.util.Collection;

public class HibernatePredicateResolver extends AbstractPredicateResolver<Criterion> {

	{
		register(JunctionPredicate.class, this::resolveJunction);
		register(ObjectPropertyPredicate.class, this::resolveObjectProperty);
		register(StringPropertyPredicate.class, this::resolveStringProperty);
	}

	Criterion resolveJunction(JunctionPredicate predicate) {
		Criterion[] criteria = predicate.predicates().stream().map(this::resolve).toArray(Criterion[]::new);
		Criterion result;
		switch (predicate.operator()) {
			case AND:
				result = Restrictions.and(criteria);
				break;
			case OR:
				result = Restrictions.or(criteria);
				break;
			default:
				throw new RuntimeException(predicate.operator().name());
		}
		return result;
	}

	Criterion resolveObjectProperty(ObjectPropertyPredicate predicate) {
		String property = predicate.property();
		Object value = predicate.value();
		//
		Criterion result;
		switch (predicate.operator()) {
			case EQUAL_TO:
				result = Restrictions.eq(property, value);
				break;
			case GREATER_THAN:
				result = Restrictions.gt(property, value);
				break;
			case GREATER_THAN_OR_EQUAL_TO:
				result = Restrictions.ge(property, value);
				break;
			case IN:
				result = Restrictions.in(property, (Collection) value);
				break;
			case LESS_THAN:
				result = Restrictions.lt(property, value);
				break;
			case LESS_THAN_OR_EQUAL_TO:
				result = Restrictions.le(property, value);
				break;
			case NOT_EQUAL_TO:
				result = Restrictions.ne(property, value);
				break;
			default:
				throw new RuntimeException(predicate.operator().name());
		}
		return result;
	}

	Criterion resolveStringProperty(StringPropertyPredicate predicate) {
		boolean caseSensitive = predicate.caseSensitive();
		String property = predicate.property();
		String value = predicate.value();
		//
		Criterion result;
		switch (predicate.operator()) {
			case CONTAINS:
				result = caseSensitive
						? Restrictions.like(property, value, MatchMode.ANYWHERE)
						: Restrictions.ilike(property, value, MatchMode.ANYWHERE);
				break;
			case ENDS_WITH:
				result = caseSensitive
						? Restrictions.like(property, value, MatchMode.END)
						: Restrictions.ilike(property, value, MatchMode.END);
				break;
			case STARTS_WITH:
				result = caseSensitive
						? Restrictions.like(property, value, MatchMode.START)
						: Restrictions.ilike(property, value, MatchMode.START);
				break;
			default:
				throw new RuntimeException(predicate.operator().name());
		}
		return result;
	}
}
