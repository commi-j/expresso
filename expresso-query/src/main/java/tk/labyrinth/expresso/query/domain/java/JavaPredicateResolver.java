package tk.labyrinth.expresso.query.domain.java;

import io.vavr.collection.Traversable;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.query.domain.common.AbstractPredicateResolver;
import tk.labyrinth.expresso.query.lang.predicate.ObjectPropertyPredicate;
import tk.labyrinth.expresso.query.lang.predicate.StringPropertyPredicate;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class JavaPredicateResolver extends AbstractPredicateResolver<Predicate<Object>> {

	private final JavaPropertyAccessor propertyAccessor;

	{
		register(JunctionPredicate.class, this::resolveJunction);
		register(ObjectPropertyPredicate.class, this::resolveObjectProperty);
		register(StringPropertyPredicate.class, this::resolveStringProperty);
	}

	public JavaPredicateResolver(JavaPropertyAccessor propertyAccessor) {
		this.propertyAccessor = propertyAccessor;
	}

	@SuppressWarnings("unchecked")
	private int compare(Object first, Object second) {
		int result;
		if (first != null) {
			if (second != null) {
				result = ((Comparable) first).compareTo(second);
			} else {
				result = 1;
			}
		} else {
			if (second != null) {
				result = -1;
			} else {
				result = 0;
			}
		}
		return result;
	}

	Predicate<Object> resolveJunction(JunctionPredicate predicate) {
		List<Predicate<Object>> predicates = predicate.predicates().stream().map(this::resolve).collect(Collectors.toList());
		Predicate<Object> result;
		switch (predicate.operator()) {
			case AND:
				result = object -> predicates.stream().allMatch(innerPredicate -> innerPredicate.test(object));
				break;
			case OR:
				result = object -> predicates.stream().anyMatch(innerPredicate -> innerPredicate.test(object));
				break;
			default:
				throw new RuntimeException(predicate.operator().name());
		}
		return result;
	}

	Predicate<Object> resolveObjectProperty(ObjectPropertyPredicate predicate) {
		UnaryOperator<Object> valueFunction = object -> propertyAccessor.get(object, predicate.property());
		Object value = predicate.value();
		//
		Predicate<Object> result;
		switch (predicate.operator()) {
			case CONTAINS:
				result = object -> {
					boolean evaluationResult;
					{
						Object objectValue = valueFunction.apply(object);
						if (objectValue != null) {
							if (objectValue instanceof Collection) {
								evaluationResult = ((Collection<?>) objectValue).contains(value);
							} else if (objectValue instanceof Traversable) {
								// TODO: Make Vavr optional dependency.
								evaluationResult = ((Traversable) objectValue).contains(value);
							} else {
								throw new IllegalStateException("Not implemented");
							}
						} else {
							evaluationResult = false;
						}
					}
					return evaluationResult;
				};
				break;
			case EQUAL_TO:
				result = object -> Objects.equals(valueFunction.apply(object), value);
				break;
			case GREATER_THAN:
				result = object -> compare(valueFunction.apply(object), value) > 0;
				break;
			case GREATER_THAN_OR_EQUAL_TO:
				result = object -> compare(valueFunction.apply(object), value) >= 0;
				break;
			case IN:
				result = object -> ((Collection<?>) value).contains(valueFunction.apply(object));
				break;
			case LESS_THAN:
				result = object -> compare(valueFunction.apply(object), value) < 0;
				break;
			case LESS_THAN_OR_EQUAL_TO:
				result = object -> compare(valueFunction.apply(object), value) <= 0;
				break;
			case NOT_EQUAL_TO:
				result = object -> !Objects.equals(valueFunction.apply(object), value);
				break;
			default:
				throw new RuntimeException(predicate.operator().name());
		}
		return result;
	}

	Predicate<Object> resolveStringProperty(StringPropertyPredicate predicate) {
		boolean caseSensitive = predicate.caseSensitive();
		String value = prepareString(predicate.value(), caseSensitive);
		//
		BiPredicate<String, String> valuePredicate;
		switch (predicate.operator()) {
			case CONTAINS:
				valuePredicate = String::contains;
				break;
			case ENDS_WITH:
				valuePredicate = String::endsWith;
				break;
			case STARTS_WITH:
				valuePredicate = String::startsWith;
				break;
			default:
				throw new RuntimeException(predicate.operator().name());
		}
		return object -> {
			String objectValue = prepareString(propertyAccessor.get(object, predicate.property()), caseSensitive);
			return objectValue != null && valuePredicate.test(objectValue, value);
		};
	}

	/**
	 * Prepares value for case argument aware checks.
	 *
	 * @param value         value to prepare, nullable
	 * @param caseSensitive whether to respect case
	 *
	 * @return nullable
	 */
	static String prepareString(String value, boolean caseSensitive) {
		return value != null
				? caseSensitive ? value : value.toUpperCase().toLowerCase()
				: null;
	}
}
