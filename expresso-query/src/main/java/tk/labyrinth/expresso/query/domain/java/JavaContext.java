package tk.labyrinth.expresso.query.domain.java;

import java.util.stream.Stream;

public interface JavaContext {

	<T> Stream<T> getStream(Class<T> type);
}
