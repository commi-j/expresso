package tk.labyrinth.expresso.query.lang.search;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Accessors(fluent = true)
@Value
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class Sort {

	public enum Direction {
		ASCENDING,
		DESCENDING
	}

	List<Entry> entries;

	public Sort then(Sort sort) {
		List<Entry> entries = new ArrayList<>(this.entries);
		entries.addAll(this.entries);
		entries.addAll(sort.entries);
		return new Sort(List.copyOf(entries));
	}

	public Sort thenAscending(String property) {
		return thenBy(property, Direction.ASCENDING);
	}

	public Sort thenBy(String property, Direction direction) {
		List<Entry> entries = new ArrayList<>(this.entries);
		entries.add(new Entry(direction, property));
		return new Sort(List.copyOf(entries));
	}

	public Sort thenDescending(String property) {
		return thenBy(property, Direction.DESCENDING);
	}

	public static Sort ascending(String property) {
		return by(property, Direction.ASCENDING);
	}

	public static Sort by(String property, Direction direction) {
		return new Sort(List.of(new Entry(direction, property)));
	}

	public static Sort concat(Sort first, Sort second) {
		return first.then(second);
	}

	public static Sort descending(String property) {
		return by(property, Direction.DESCENDING);
	}

	@Accessors(fluent = true)
	@Data
	@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
	public static class Entry {

		@NonNull
		private final Direction direction;

		@NonNull
		private final String property;
	}
}
