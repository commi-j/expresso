package tk.labyrinth.expresso.query.domain.hibernate;

import org.hibernate.Criteria;
import org.hibernate.SharedSessionContract;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import tk.labyrinth.expresso.query.domain.common.QueryExecutor;
import tk.labyrinth.expresso.query.domain.common.util.QueryExecutorUtils;
import tk.labyrinth.expresso.query.lang.search.SearchQuery;
import tk.labyrinth.expresso.query.lang.search.Sort;

import java.util.List;

public class HibernateQueryExecutor implements QueryExecutor<HibernateContext> {

	private final HibernatePredicateResolver predicateResolver = new HibernatePredicateResolver();

	@SuppressWarnings("unchecked")
	private <T> List<T> cast(List list) {
		return list;
	}

	@SuppressWarnings("deprecation")
	private Criteria getCriteria(SharedSessionContract session, Class<?> type) {
		return session.createCriteria(type);
	}

	public Criteria buildCriteria(HibernateContext context, SearchQuery<?> query, boolean search) {
		Criteria result = getCriteria(context.getSession(), query.getType());
		if (query.getPredicate() != null) {
			result = result.add(predicateResolver.resolve(query.getPredicate()));
		}
		if (search) {
			if (query.getLimit() != null) {
				result = result.setMaxResults(query.getLimit().intValue());
			}
			if (query.getOffset() != null) {
				result = result.setFirstResult(query.getOffset().intValue());
			}
			if (query.getSort() != null) {
				for (Sort.Entry sortEntry : query.getSort().entries()) {
					result = result.addOrder(buildOrder(sortEntry));
				}
			}
		}
		return result;
	}

	public Order buildOrder(Sort.Entry sortEntry) {
		Sort.Direction direction = sortEntry.direction();
		String property = sortEntry.property();
		//
		Order result;
		switch (direction) {
			case ASCENDING:
				result = Order.asc(property);
				break;
			case DESCENDING:
				result = Order.desc(property);
				break;
			default:
				throw new IllegalStateException(String.valueOf(direction));
		}
		return result;
	}

	@Override
	public long count(HibernateContext context, SearchQuery<?> query) {
		return QueryExecutorUtils.adjustCount((long) buildCriteria(context, query, false).setProjection(Projections.rowCount()).uniqueResult(), query.getOffset(), query.getLimit());
	}

	@Override
	public <T> List<T> search(HibernateContext context, SearchQuery<T> query) {
		return cast(buildCriteria(context, query, true).list());
	}
}
