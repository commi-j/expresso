package tk.labyrinth.expresso.query.domain.common.util;

public class QueryExecutorUtils {

	public static long adjustCount(long count, Long offset, Long limit) {
		long offsetCount = offset != null ? Math.max(count - offset, 0) : count;
		return limit != null ? Math.min(offsetCount, limit) : offsetCount;
	}
}
