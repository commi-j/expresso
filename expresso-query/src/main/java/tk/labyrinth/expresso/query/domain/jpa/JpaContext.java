package tk.labyrinth.expresso.query.domain.jpa;

import lombok.Data;

import javax.persistence.EntityManager;

@Data
public class JpaContext {

	private final EntityManager entityManager;
}
