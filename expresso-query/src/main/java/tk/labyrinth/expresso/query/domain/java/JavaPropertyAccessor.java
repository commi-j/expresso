package tk.labyrinth.expresso.query.domain.java;

public interface JavaPropertyAccessor {

	<T> T get(Object target, String property);
}
