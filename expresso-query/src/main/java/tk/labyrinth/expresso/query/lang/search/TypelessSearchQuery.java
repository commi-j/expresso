package tk.labyrinth.expresso.query.lang.search;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import tk.labyrinth.expresso.lang.predicate.Predicate;

/**
 * @see SearchQuery
 */
@Data
public class TypelessSearchQuery {

	private final Long limit;

	private final Long offset;

	private final Predicate predicate;

	private final Sort sort;

	public <T> SearchQuery<T> asTypeful(Class<T> type) {
		return SearchQuery.builder(type)
				.limit(limit)
				.offset(offset)
				.predicate(predicate)
				.sort(sort)
				.build();
	}

	public Sort getSort() {
		// FIXME: Temporal solution to hide QueryExecutor javadoc error.
		return this.sort;
	}

	public Builder toBuilder() {
		return new Builder()
				.limit(limit)
				.offset(offset)
				.predicate(predicate)
				.sort(sort);
	}

	public static Builder builder() {
		return new Builder();
	}

	@Accessors(fluent = true)
	@Data
	@RequiredArgsConstructor
	public static class Builder {

		private Long limit = null;

		private Long offset = null;

		private Predicate predicate = null;

		private Sort sort = null;

		public TypelessSearchQuery build() {
			return new TypelessSearchQuery(limit, offset, predicate, sort);
		}
	}
}
