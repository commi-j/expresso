package tk.labyrinth.expresso.query.domain.common;

import tk.labyrinth.expresso.lang.predicate.Predicate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class AbstractPredicateResolver<T> {

	private final Function<? extends Predicate, T> noResolver = value -> {
		throw new UnsupportedOperationException();
	};

	private final Map<Class<? extends Predicate>, Function<? extends Predicate, T>> cache = new HashMap<>();

	private final Map<Class<? extends Predicate>, Function<? extends Predicate, T>> resolvers = new HashMap<>();

	Function<? extends Predicate, T> getResolver(Class<? extends Predicate> type) {
		// TODO: Thread-safety for resolvers & cache.
		Function<? extends Predicate, T> result = cache.get(type);
		if (result == null) {
			List<Function<? extends Predicate, T>> match = resolvers.entrySet().stream().filter(entry -> entry.getKey().isAssignableFrom(type))
					.map(Map.Entry::getValue)
					.collect(Collectors.toList());
			if (match.size() == 1) {
				result = match.get(0);
				cache.put(type, result);
			} else {
				cache.put(type, noResolver);
				if (match.isEmpty()) {
					throw new RuntimeException("No Resolver matches Type = " + type.getName());
				} else {
					throw new RuntimeException("Multiple Resolvers match Type = " + type.getName() + ": " + match);
				}
			}
		} else if (result == noResolver) {
			throw new RuntimeException("No Resolver found for Type = " + type.getName());
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	<P extends Predicate> T resolve(Function<P, T> resolver, Predicate predicate) {
		return resolver.apply((P) predicate);
	}

	protected final <P extends Predicate> void register(Class<P> type, Function<P, T> resolver) {
		resolvers.put(type, resolver);
	}

	public T resolve(Predicate predicate) {
		return resolve(getResolver(predicate.getClass()), predicate);
	}
}
