package tk.labyrinth.expresso.query.domain.mongodb;

import lombok.Getter;
import org.springframework.data.mongodb.core.query.Query;
import tk.labyrinth.expresso.query.domain.common.QueryExecutor;
import tk.labyrinth.expresso.query.domain.common.util.QueryExecutorUtils;
import tk.labyrinth.expresso.query.lang.search.SearchQuery;
import tk.labyrinth.expresso.query.lang.search.Sort;
import tk.labyrinth.expresso.query.lang.search.TypelessSearchQuery;

import java.util.List;
import java.util.stream.Collectors;

public class MongoDbQueryExecutor implements QueryExecutor<MongoDbContext> {

	@Getter
	private final MongoDbPredicateResolver predicateResolver = new MongoDbPredicateResolver();

	public Query buildQuery(TypelessSearchQuery query, boolean search) {
		Query result = new Query();
		if (query.getPredicate() != null) {
			result = result.addCriteria(predicateResolver.resolve(query.getPredicate()));
		}
		if (search) {
			if (query.getLimit() != null) {
				result = result.limit(query.getLimit().intValue());
			}
			if (query.getOffset() != null) {
				result = result.skip(query.getOffset());
			}
			if (query.getSort() != null) {
				result = result.with(buildSort(query.getSort()));
			}
		}
		return result;
	}

	public org.springframework.data.domain.Sort buildSort(Sort sort) {
		return org.springframework.data.domain.Sort.by(sort.entries().stream().map(sortEntry -> {
			Sort.Direction direction = sortEntry.direction();
			String property = sortEntry.property();
			//
			org.springframework.data.domain.Sort.Order result;
			switch (direction) {
				case ASCENDING:
					result = org.springframework.data.domain.Sort.Order.asc(property);
					break;
				case DESCENDING:
					result = org.springframework.data.domain.Sort.Order.desc(property);
					break;
				default:
					throw new IllegalArgumentException(String.valueOf(direction));
			}
			return result;
		}).collect(Collectors.toList()));
	}

	@Override
	public long count(MongoDbContext context, SearchQuery<?> query) {
		return QueryExecutorUtils.adjustCount(
				context.getTemplate().count(
						buildQuery(query.asTypeless(), false),
						query.getType()),
				query.getOffset(),
				query.getLimit());
	}

	@Override
	public <T> List<T> search(MongoDbContext context, SearchQuery<T> query) {
		return context.getTemplate().find(
				buildQuery(query.asTypeless(), true),
				query.getType());
	}
}
