package tk.labyrinth.expresso.query.domain.common;

import tk.labyrinth.expresso.query.lang.search.SearchQuery;

import java.util.List;

/**
 * Implementation of this interface is capable of executing read/write queries.<br>
 * The implementation is expected to be data-independent - any data-related information should be provided via context.<br>
 *
 * @param <C> data context for this executor
 */
public interface QueryExecutor<C> {

	/**
	 * Performs a count operation with specified {@link SearchQuery}.<br>
	 * {@link SearchQuery#getSort()} may be ignored by this method due to no effect on result.<br>
	 *
	 * @param context non-null
	 * @param query   non-null
	 *
	 * @return non-negative
	 */
	long count(C context, SearchQuery<?> query);

	/**
	 * Performs a search operation with specified {@link SearchQuery}.<br>
	 *
	 * @param context non-null
	 * @param query   non-null
	 * @param <T>     entity type
	 *
	 * @return non-null
	 */
	<T> List<T> search(C context, SearchQuery<T> query);
}
