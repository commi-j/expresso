package tk.labyrinth.expresso.query.domain.hibernate;

import lombok.Data;
import org.hibernate.SharedSessionContract;

@Data
public class HibernateContext {

	private final SharedSessionContract session;
}
