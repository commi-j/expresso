package tk.labyrinth.expresso.query.domain.mongodb;

import lombok.Data;
import org.springframework.data.mongodb.core.MongoTemplate;

@Data
public class MongoDbContext {

	private final MongoTemplate template;
}
