package tk.labyrinth.expresso.query.domain.java;

import org.springframework.beans.BeanUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

public class SpringPropertyAccessor implements JavaPropertyAccessor {

	@Override
	public <T> T get(Object target, String property) {
		PropertyDescriptor propertyDescriptor = BeanUtils.getPropertyDescriptor(target.getClass(), property);
		if (propertyDescriptor != null) {
			try {
				Method method = propertyDescriptor.getReadMethod();
				if (method != null) {
					return cast(method.invoke(target));
				} else {
					throw new IllegalStateException("No ReadMethod found for target = " + target + ", property = " + property);
				}
			} catch (ReflectiveOperationException ex) {
				throw new RuntimeException(ex);
			}
		} else {
			throw new IllegalStateException("No PropertyDescriptor found for target = " + target + ", property = " + property);
		}
	}

	@SuppressWarnings("unchecked")
	private static <T> T cast(Object value) {
		return (T) value;
	}
}
