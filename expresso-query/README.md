## Expresso Query

[![MAVEN-CENTRAL](https://img.shields.io/maven-central/v/tk.labyrinth/expresso-query.svg?label=Maven+Central)](https://mvnrepository.com/artifact/tk.labyrinth/expresso-query)
[![LICENSE](https://img.shields.io/badge/license-MIT-green.svg?label=License)](LICENSE)

Tiny library for building unified search queries for MongoDB, Hibernate and Java (Streams). 

## Maven Dependency

```xml
	<dependency>
		<groupId>tk.labyrinth</groupId>
		<artifactId>expresso-query</artifactId>
		<version>1.1.4</version>
	</dependency>
```

## Getting Started

#### Hibernate Setup

```java
HibernateQueryExecutor queryExecutor = new HibernateQueryExecutor();
org.hibernate.SharedSessionContract session = ...
HibernateContext context = new HibernateContext(session);
```

#### Java Setup

```java
JavaQueryExecutor queryExecutor = new JavaQueryExecutor(new SpringPropertyAccessor());
JavaContext context = ...
```

#### MongoDB Setup

```java
MongoDbQueryExecutor queryExecutor = new MongoDbQueryExecutor();
org.springframework.data.mongodb.core.MongoTemplate template = ...
MongoDbContext context = new MongoDbContext(template);
```

#### Invocation

```java
List<MyObject> equalToSearchResult = queryExecutor.search(context, SearchQuery.builder(MyObject.class)
		.predicate(Predicates.equalTo("someField", "someValue"))
		.build());
//
List<MyObject> caseInsensitiveContainsSearchResult = queryExecutor.search(context, SearchQuery.builder(MyObject.class)
		.predicate(Predicates.contains("someField", "someValue", false))
		.build());
//
List<MyObject> pageResult = queryExecutor.search(context, SearchQuery.builder(MyObject.class)
		.sort(Sort.ascending("groupId").thenDescending("name"))
		.offset(200L)
		.limit(50L)
		.build());
//
long count = queryExecutor.count(context, SearchQuery.builder(MyObject.class)
		.predicate(Predicates.and(
				Predicates.greaterThanOrEqualTo("someDate", LocalDate.of(2018, 12, 30)),
				Predicates.lessThan("someDate", LocalDate.of(2019, 1, 10))))
		.build());
```
